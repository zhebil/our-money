import { InputsGroup } from './components/inputs-group';

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <InputsGroup/>
        </div>
      </div>
    </div>
  );
}

export default App;
