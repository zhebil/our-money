import firebase from "firebase/app";

import React, { createContext } from "react";
import ReactDOM from "react-dom";
import App from "./App";

import "firebase/firestore";

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firestore = firebase.firestore();

export interface firebaseContext {
  firebaseApp: firebase.app.App;
  firestore: firebase.firestore.Firestore;
}
export const FirebaseContext = createContext<firebaseContext>({
  firebaseApp,
  firestore,
});

ReactDOM.render(
  <React.StrictMode>
    <FirebaseContext.Provider
      value={{
        firebaseApp,
        firestore,
      }}
    >
      <App />
    </FirebaseContext.Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
