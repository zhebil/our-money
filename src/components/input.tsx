import React, { useState } from "react";
import { useContext } from "react";
import { FirebaseContext } from "../";
import firebase from "firebase/app";

type InputProps = {
  name: string;
  label: string;
  price: number;
  type: "jar" | "income" | "spend";
  lastUpdate: any;
  lastSum: number;
};

function Input({ name, label, price, type, lastUpdate, lastSum }: InputProps) {
  const [value, setValue] = useState<string>("");
  const { firestore } = useContext(FirebaseContext);

  const addSpending = (e: React.FormEvent) => {
    e.preventDefault();
    if (isNaN(Number(value))) {
      return;
    }
    if (!value.trim().length) {
      return;
    }

    firestore.collection(type).add({
      type: name,
      sum: value,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    });

    setValue("");
  };

  return (
    <div className="mb-4">
      <div className="mb-2">
        <h2 className="m-0">{label}</h2>

        <div>
          Сумма: <span className="h3">{price}</span>
        </div>
        <div>
          <div>
            Последнее обновление: {new Date(lastUpdate).toLocaleDateString()}
          </div>
          <div>Последняя сума: {lastSum}</div>
        </div>
      </div>
      <form onSubmit={addSpending} className="col-8">
        <input
          value={value}
          onChange={(e) => setValue(e.target.value)}
          className="form-control mb-2"
          placeholder="Введите трату"
          type="text"
        />
        <button className="btn btn-primary">Добавить</button>
      </form>
    </div>
  );
}

export default Input;
