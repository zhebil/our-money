import React from "react";
import Input from "./input";
import { useContext } from "react";
import { FirebaseContext } from "../";
import { useCollectionData } from "react-firebase-hooks/firestore";
import { Loader } from "./loader";

function InputsGroup() {
  const { firestore } = useContext(FirebaseContext);

  const [values, loading, error] = useCollectionData<value>(
    firestore.collection("spend").orderBy("createdAt")
  );

  const [incomes, loadingIncomes, errorIncomes] = useCollectionData<value>(
    firestore.collection("income").orderBy("createdAt")
  );

  const [jarWithdraw, loadingJar, errorJar] = useCollectionData<value>(
    firestore.collection("jar").orderBy("createdAt")
  );

  interface value {
    type: keyof typeof spentDictionary;
    sum: number;
    createdAt: any;
  }

  type Dictionary = {
    [key: string]: {
      label: string;
      values: value[];
      sum: number;
      lastUpdated: any;
      lastSum: number;
    };
  };
  let total = 0;
  const spentDictionary: Dictionary = {
    travel: {
      label: "Проезд",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    decorations: {
      label: "Украшения",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    grocery: {
      label: "Продукты",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    coffee: {
      label: "Кофе",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    beer: {
      label: "Пиво",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    cafe: {
      label: "Кафе и рестораны",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    teeth: {
      label: "Зубы",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    meds: {
      label: "Лекарства",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    clothes: {
      label: "Одежда",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    car: {
      label: "Машина",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    post: {
      label: "Новая почта",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    hairCut: {
      label: "Стрижка",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    nails: {
      label: "Ногти",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    sugaring: {
      label: "Шугаринг",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    taxi: {
      label: "Такси",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    entertainment: {
      label: "Развлечения",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    presents: {
      label: "Подарки",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    utilities: {
      label: "Комуналка",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    mobiles: {
      label: "Мобильные",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
  };

  const incomeDictionary: Dictionary = {
    salary: {
      label: "Зарплата",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    cashBack: {
      label: "Кешбек",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
  };
  const jarDictionary: Dictionary = {
    jarWithdraw: {
      label: "Банка снятие",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
    jarSavings: {
      label: "Банка отложено",
      values: [],
      sum: 0,
      lastUpdated: "",
      lastSum: 0,
    },
  };

  if (loading || loadingIncomes || loadingJar) {
    return <Loader />;
  }

  if (error || errorIncomes || errorJar) {
    console.log(error, errorIncomes, errorJar);
    return <h1>Произошла какая-то ошибка</h1>;
  }

  values?.forEach(
    (
      i: { type: keyof typeof spentDictionary; sum: number; createdAt: any },
      idx
    ) => {
      spentDictionary[i.type].values.push(i);
      spentDictionary[i.type].sum += Number(i.sum);
      total += Number(i.sum);
      spentDictionary[i.type].lastUpdated = i.createdAt?.toDate();
      spentDictionary[i.type].lastSum = i.sum;
    }
  );

  incomes?.forEach(
    (
      i: {
        type: keyof typeof incomeDictionary;
        sum: number;
        createdAt: any;
      },
      idx
    ) => {
      incomeDictionary[i.type].values.push(i);
      incomeDictionary[i.type].sum += Number(i.sum);
      incomeDictionary[i.type].lastUpdated = i.createdAt?.toDate();
      incomeDictionary[i.type].lastSum = i.sum;
    }
  );
  jarWithdraw?.forEach(
    (
      i: { type: keyof typeof jarDictionary; sum: number; createdAt: any },
      idx
    ) => {
      jarDictionary[i.type].values.push(i);
      jarDictionary[i.type].sum += Number(i.sum);
      jarDictionary[i.type].lastUpdated = i.createdAt?.toDate();
      jarDictionary[i.type].lastSum = i.sum;
    }
  );

  return (
    <div className="p-4">
      <h1 className="text-primary">Траты</h1>
      {Object.keys(spentDictionary).map((key) => {
        return (
          <Input
            type="spend"
            key={key}
            name={key}
            label={spentDictionary[key].label}
            price={spentDictionary[key].sum}
            lastUpdate={spentDictionary[key].lastUpdated}
            lastSum={spentDictionary[key].lastSum}
          />
        );
      })}
      <p className="h1 text-danger">Общая потраченая сумма: {total}</p>
      <h1 className="text-primary">Поступления</h1>
      {Object.keys(incomeDictionary).map((key) => {
        return (
          <Input
            type="income"
            key={key}
            name={key}
            label={incomeDictionary[key].label}
            price={incomeDictionary[key].sum}
            lastUpdate={incomeDictionary[key].lastUpdated}
            lastSum={incomeDictionary[key].lastSum}
          />
        );
      })}
      <h1 className="text-primary">Банка</h1>
      {Object.keys(jarDictionary).map((key) => {
        return (
          <Input
            type="jar"
            key={key}
            name={key}
            label={jarDictionary[key].label}
            price={jarDictionary[key].sum}
            lastUpdate={jarDictionary[key].lastUpdated}
            lastSum={jarDictionary[key].lastSum}
          />
        );
      })}
    </div>
  );
}

export { InputsGroup };
